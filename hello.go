package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func sayHello() string {
	return "Hello, world."
}

func main() {
	engine := gin.Default()
	engine.GET("/hello", func(context *gin.Context) {
		context.JSON(http.StatusOK, gin.H{
			"message": sayHello(),
		})
	})
	engine.Run()
}
